import {Alert} from 'react-native';

//Search All
async function searchAll(param) {
  try {
    const res = await fetch(
      'https://deezerdevs-deezer.p.rapidapi.com/search?q=' + param,
      {
        method: 'GET',
        headers: {
          'x-rapidapi-hos': 'deezerdevs-deezer.p.rapidapi.com',
          'x-rapidapi-key':
            '4da6841bf8mshd35306bbc12ad22p11f3d7jsn0a98e1085a30',
        },
      },
    );
    const data = await res.json();
    console.log(data.data[0].album.cover_small); //small cover img
    //console.log(data.data);
    return data;
  } catch (error) {
    console.log(error);
    Alert.alert('Try again');
  }
}

// To get Select Track
async function getTrack(id) {
  try {
    const res = await fetch(
      'https://deezerdevs-deezer.p.rapidapi.com/track/' + id,
      {
        method: 'GET',
        headers: {
          'x-rapidapi-hos': 'deezerdevs-deezer.p.rapidapi.com',
          'x-rapidapi-key':
            '4da6841bf8mshd35306bbc12ad22p11f3d7jsn0a98e1085a30',
        },
      },
    );
    const data = await res.json();
    console.log('Success', data);
    console.log(data.preview);
  } catch (error) {
    Alert.alert('Oops', [
      {
        text: 'OK',
        onPress: async () => {
          await handleUnauthorized;
        },
      },
    ]);
  }
}

async function handleUnauthorized() {
  try {
    console.log('logout');
  } catch {
    console.log('logout');
  }
}

module.exports.searchAll = searchAll;
module.exports.getTrack = getTrack;
