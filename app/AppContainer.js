import React from 'react';
import {View, Text, Button} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Onboarding from '../app/Onboarding';
import Playlist from '../app/Playlist';
import Player from '../app/Player';
import App from '../app/App';
import Header from '../app/Header';
import Signin from '../app/Signin';
import Signup from '../app/Signup';

const AppNavigator = createStackNavigator(
  {
    Onboarding: Onboarding,
    Playlist: Playlist,
    Player: Player,
    App: App,
    Signin: Signin,
    Signup: Signup,
  },
  {
    initialRouteName: 'Onboarding',
    headerMode: 'none',
  },
);

const AppContainerAppContainer = createAppContainer(AppNavigator);
export default class AppContainer extends React.Component {
  render() {
    return <AppContainerAppContainer />;
  }
}
