import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  Alert,
  Image,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import Images from '../constants/Images';
import auth from '@react-native-firebase/auth';

export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {userid: '', password: '', animating: false};
  }

  loginUser = () => {
    this.setState({animating: true});
    if (this.state.userid == '') {
      Alert.alert('Invalid, Email cannot be blank.');
      this.setState({animating: false});
    } else if (this.state.password == '') {
      Alert.alert('Invalid, Passowrd cannot be blank');
      this.setState({animating: false});
    } else {
      auth()
        .signInWithEmailAndPassword(this.state.userid, this.state.password)
        .then(() => {
          this.props.navigation.navigate('Playlist');
          this.setState({animating: false});
          console.log('User account created & signed in!');
        })
        .catch((error) => {
          if (error.code === 'auth/email-already-in-use') {
            Alert.alert('Email Already In Use');
            this.setState({animating: false});
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            Alert.alert('Email Already Invalid');
            this.setState({animating: false});
            console.log('That email address is invalid!');
          }
          this.setState({animating: false});
          Alert.alert('Invalid Action');
          console.error(error);
        });
    }
  };

  render() {
    let navigation = this.props.navigation;
    const animating = this.state.animating;
    return (
      <SafeAreaView
        style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
        {this.state.animating ? (
          <ActivityIndicator
            size="large"
            animating={animating}
            color="#bc2b78"
            size="large"
          />
        ) : (
          <ScrollView style={{padding: 30}}>
            <View style={{alignItems: 'center'}}>
              <Image style={{padding: 15}} source={Images.Logo} />
            </View>
            <Text style={{fontSize: 27, paddingTop: 20, paddingBottom: 20}}>
              Login
            </Text>
            <TextInput
              style={{padding: 20}}
              placeholder="Username"
              onChangeText={(userid) => this.setState({userid})}
              value={this.state.userid}
            />
            <TextInput
              style={{padding: 20}}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
            />
            <View style={{margin: 10}} />

            <View style={{paddingTop: 30}}>
              <Button onPress={this.loginUser} title="Sign In" />
            </View>
            <View style={{paddingTop: 30}}>
              <Button
                title="Do not have an Account?"
                onPress={() => this.props.navigation.navigate('Signup')}
              />
            </View>
            <View style={{paddingTop: 30}}>
              <Button
                title="Back"
                onPress={() => this.props.navigation.navigate('Onboarding')}
              />
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}
