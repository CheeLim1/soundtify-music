import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  Image,
  Alert,
  Button,
} from 'react-native';
import {SearchBar} from 'react-native-elements';

export default class Playlist extends React.Component {
  constructor(props) {
    super(props);
    //setting default state
    this.state = {animating: true, search: 'BTS', dataSource: ''};
    this.arrayholder = [];
  }
  componentDidMount() {
    var text = 'BTS';
    this.searchArtistAPI(text);
  }

  searchArtistAPI(text) {
    return fetch('https://deezerdevs-deezer.p.rapidapi.com/search?q=' + text, {
      method: 'GET',
      headers: {
        'x-rapidapi-hos': 'deezerdevs-deezer.p.rapidapi.com',
        'x-rapidapi-key': '4da6841bf8mshd35306bbc12ad22p11f3d7jsn0a98e1085a30',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const result = responseJson;
        this.setState(
          {
            animating: false,
            dataSource: result.data,
          },
          function () {
            this.arrayholder = result.data;
          },
        );
      })
      .catch((error) => {
        Alert.alert(
          'Not able to find artist, Please search the artist in the search bar',
        );
        //console.error(error);
      });
  }

  search = (text) => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    this.setState({
      search: text,
    });
    this.searchArtistAPI(text);
  }
  ListViewItemSeparator = () => {
    //Item sparator view
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  render() {
    let navigation = this.props.navigation;
    let {animating} = this.state.animating;
    return (
      <SafeAreaView
        style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
        {this.state.animating ? (
          <ActivityIndicator
            size="large"
            animating={animating}
            color="#bc2b78"
            size="large"
          />
        ) : (
          <View style={styles.viewStyle}>
            <SearchBar
              round
              searchIcon={{size: 24}}
              onChangeText={(text) => this.SearchFilterFunction(text)}
              onClear={(text) => this.SearchFilterFunction('')}
              placeholder="Search Artist Album's Title"
              value={this.state.search}
            />
            <FlatList
              data={this.state.dataSource}
              ItemSeparatorComponent={this.ListViewItemSeparator}
              //Item Separator View
              renderItem={({item}) => (
                <Button
                  color="black"
                  title={item.title}
                  onPress={() =>
                    this.props.navigation.navigate('App', {
                      artist: this.state.search,
                    })
                  }
                />
              )}
              enableEmptySections={true}
              style={{marginTop: 10}}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'black',
    marginTop: Platform.OS == 'ios' ? 30 : 0,
  },
  textStyle: {
    padding: 10,
    color: 'white',
    justifyContent: 'flex-start',
  },
});
