import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  Alert,
  Image,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import Images from '../constants/Images';

export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {userid: '', password: '', animating: false};
  }

  createUser = () => {
    this.setState({animating: true});
    if (this.state.userid == '') {
      Alert.alert('Invalid, Email cannot be blank.');
      this.setState({animating: false});
    } else if (this.state.password == '') {
      Alert.alert('Invalid, Passowrd cannot be blank');
      this.setState({animating: false});
    } else {
      auth()
        .createUserWithEmailAndPassword(this.state.userid, this.state.password)
        .then(() => {
          console.log('User account created & signed in!');
          Alert.alert('User Account has been created, proceed to login');
          this.setState({animating: false});
          this.props.navigation.navigate('Signin');
        })
        .catch((error) => {
          if (error.code === 'auth/email-already-in-use') {
            Alert.alert('That email address is already in use!');
            this.setState({animating: false});
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            Alert.alert('That email address is invalid!');
            this.setState({animating: false});
            console.log('That email address is invalid!');
          }
          this.setState({animating: false});
          Alert.alert('Invalid Action');
          console.error(error);
        });
    }
  };

  render() {
    let navigation = this.props.navigation;
    const animating = this.state.animating;
    return (
      <SafeAreaView
        style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
        {this.state.animating ? (
          <ActivityIndicator
            size="large"
            animating={animating}
            color="#bc2b78"
            size="large"
          />
        ) : (
          <ScrollView style={{padding: 30}}>
            <ActivityIndicator />
            <View style={{alignItems: 'center'}}>
              <Image style={{padding: 15}} source={Images.Logo} />
            </View>
            <Text style={{fontSize: 27, paddingTop: 20, paddingBottom: 20}}>
              Sign Up
            </Text>
            <TextInput
              style={{padding: 20}}
              placeholder="Username"
              onChangeText={(userid) => this.setState({userid})}
              value={this.state.userid}
            />
            <TextInput
              style={{padding: 20}}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
            />
            <View style={{margin: 10}} />
            <View style={{paddingTop: 30}}>
              <Button onPress={this.createUser} title="Sign Up" />
            </View>

            <View style={{paddingTop: 30}}>
              <Text style={{alignSelf: 'center', paddingBottom: 7}}>
                Already have an account?
              </Text>
              <Button
                title="Sign In"
                onPress={() => this.props.navigation.navigate('Signin')}
              />
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}
