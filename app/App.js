import React, {Component} from 'react';
import Player from './Player';
import {Alert} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anotherArray: [{}],
    };
    this.params = this.props.navigation.state.params;
    this.artist = this.params.artist;
  }

  async componentWillMount() {
    await this.getTrack();
  }

  async getTrack() {
    try {
      const res = await fetch(
        'https://deezerdevs-deezer.p.rapidapi.com/search?q=' + this.artist,
        {
          method: 'GET',
          headers: {
            'x-rapidapi-hos': 'deezerdevs-deezer.p.rapidapi.com',
            'x-rapidapi-key':
              '4da6841bf8mshd35306bbc12ad22p11f3d7jsn0a98e1085a30',
          },
        },
      );
      const result = await res.json();
      var data = await result.data;
      //var artist = data[0].artist.name; //get artist name
      var objectJson = {},
        emptyArray = [];

      data.forEach(
        await function (item) {
          objectJson = {
            audioUrl: item.preview,
            title: item.album.title,
            artist: item.artist.name,
            albumArtUrl: item.album.cover_big,
          };
          emptyArray.push(objectJson);
        },
      );
      this.setState({anotherArray: emptyArray});
    } catch (error) {
      Alert.alert('Not able to find artist');
      //console.log(error);
    }
  }

  render() {
    const {anotherArray} = this.state;

    return <Player tracks={anotherArray} />;
  }
}
