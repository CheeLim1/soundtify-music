# React Native Music Preview Streaming App

<p>
  <!-- iOS -->
  <img alt="Supports Expo iOS" longdesc="Supports Expo iOS" src="https://img.shields.io/badge/iOS-4630EB.svg?style=flat-square&logo=APPLE&labelColor=999999&logoColor=fff" />
  <!-- Android -->
  <img alt="Supports Expo Android" longdesc="Supports Expo Android" src="https://img.shields.io/badge/Android-4630EB.svg?style=flat-square&logo=ANDROID&labelColor=A4C639&logoColor=fff" />
  <!-- Web -->
  <img alt="Supports Expo Web" longdesc="Supports Expo Web" src="https://img.shields.io/badge/web-4630EB.svg?style=flat-square&logo=GOOGLE-CHROME&labelColor=4285F4&logoColor=fff" />
</p>

## 🚀 How to use

- cd project folder
- Run `npm install`
- Run `react-native run-Android` for Android

## 📝 Notes

- [Intergated with Firebase Auth](https://rnfirebase.io/auth/usage)
- [Firebase Setup](https://rnfirebase.io/)
- [Rapid API Music Library](https://rapidapi.com/deezerdevs/api/deezer-1)
