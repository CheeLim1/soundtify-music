// local imgs
const Onboarding = require('../assets/imgs/bg.png');
const LogoOnboarding = require('../assets/imgs/soundtify-onboarding.png');
const ProfileBackground = require('../assets/imgs/profile-screen-bg.png');
const RegisterBackground = require('../assets/imgs/register-bg.png');
const Pro = require('../assets/imgs/getPro-bg.png');
const iOSLogo = require('../assets/imgs/ios.png');
const androidLogo = require('../assets/imgs/android.png');
const Logo = require('../assets/imgs/logo.png');

export default {
  Onboarding,
  LogoOnboarding,
  ProfileBackground,
  RegisterBackground,
  Pro,
  iOSLogo,
  androidLogo,
  Logo,
};
