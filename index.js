import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import App from './app/AppContainer';

AppRegistry.registerComponent('Soundtify', () => App);
